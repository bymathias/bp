'use strict'

# Grunt config wrapper function
module.exports = (grunt) ->

  # Configuration
  pathConfig =
    src: 'src'
    dist:
      root: '.'
      asset: 'assets'
    test: 'test'
    doc: 'doc'

  # Load all grunt tasks
  require('matchdep').filterDev('{grunt-*,intern}').forEach grunt.loadNpmTasks

  # =================================
  # Initialize our configuration object
  grunt.initConfig

    # Metadata and config
    pkg: grunt.file.readJSON 'package.json'
    cpt: grunt.file.readJSON 'bower.json'
    cfg: pathConfig

    # Build dynamic banner
    meta:
      banner: '''
      /*! <%= grunt.template.today("d/m/yyyy") %>
       * <%= pkg.title %> v<%= pkg.version %>
       * Copyright (c) <%= grunt.template.today("yyyy") %>
       * Author: <%= pkg.author.name %>
       * License: <%= pkg.licenses.type %> */\n
      '''

    # ---------------------------------
    # Concatenate to built JS files
    concat:
      mainJs:
        # options:
        #   separator: ';'
        dest: '<%= cfg.dist.asset %>/js/main.js'
        src: [
          '<%= cfg.src %>/scripts/plugins/log.js',
          # 'bower_components/bootstrap/js/dropdown.js',
          '<%= cfg.src %>/scripts/plugins/!(_)*.js',
          '<%= cfg.src %>/scripts/helper.js',
          '<%= cfg.src %>/scripts/main.js'
        ]

    # ---------------------------------
    # Compile Stylus files to CSS
    stylus:
      mainCss:
        options:
          compress: false
          urlfunc: 'embedurl'
        files:
          '<%= cfg.dist.asset %>/css/main.css': '<%= cfg.src %>/styles/main.css.styl'

    # ---------------------------------
    # Lint JS files with JSHint
    jshint:
      options:
        # doc: jshint.com/docs
        jshintrc: '.jshintrc'
      dist: '<%= cfg.dist.asset %>/js/*.js'
      source: '<%= cfg.src %>/scripts/*.js'

    # ---------------------------------
    # Validate files with Google Linter
    gjslint:
      options:
        reporter:
          name: 'console'
        force: true
      dist:
        src: '<%= jshint.dist %>'
      source:
        src: '<%= jshint.source %>'

    # ---------------------------------
    # Lint CSS files with CSSLint
    csslint:
      options:
        # doc: github.com/stubbornella/csslint/wiki/Rules
        csslintrc: '.csslintrc'
      dist: '<%= cfg.dist.asset %>/css/*.css'

    # ---------------------------------
    # Sorting CSS properties via CSScomb
    csscomb:
      dist:
        files:
          '<%= cfg.dist.asset %>/css/main.css': [ '<%= cfg.dist.asset %>/css/main.css' ]

    # ---------------------------------
    # Minify JS files with Uglify
    uglify:
      options:
        banner: '<%= meta.banner %>'
        # mangle: false
      main:
        files:
          '<%= cfg.dist.asset %>/js/main.<%= pkg.version %>.min.js': '<%= concat.mainJs.dest %>'

    # ---------------------------------
    # Minify CSS files with Clean-css
    cssmin:
      options:
        banner: '<%= meta.banner %>'
      main:
        src: '<%= cfg.dist.asset %>/css/main.css'
        dest: '<%= cfg.dist.asset %>/css/main.<%= pkg.version %>.min.css'

    # ---------------------------------
    # Testing with intern
    intern:
      client:
        options:
          config: '<%= cfg.test %>/intern'

    # ---------------------------------
    # Configure and export a custom Modernizr build (modernizr.com/download)
    modernizr:
      extra:
        shiv: true
        printshiv: false
        load: true
        mq: false
        cssclasses: true
      extensibility:
        addtest: false
        prefixed: false
        teststyles: false
        testprops: false
        testallprops: false
        hasevents: false
        prefixes: false
        domprefixes: false
      # uglify: false
      parseFiles: false
      devFile: 'bower_components/modernizr/modernizr.dev.js'
      outputFile: '<%= cfg.dist.asset %>/js/libs/modernizr.<%= cpt.dependencies.modernizr %>.min.js'
      files: [
        '<%= cfg.dist.asset %>/js/*.js',
        '<%= cfg.dist.asset %>/css/*.css'
      ]

    # ---------------------------------
    # Minify images using OptiPNG and jpegtran
    imagemin:
      dist:
        options:
          optimizationLevel: 3
        expand: true
        cwd: '<%= cfg.dist.asset %>/img'
        dest: '<%= cfg.dist.asset %>/img'
        src: [ '**/*.{png,jpg,jpeg}' ]

    # Optimizes PNG and JPG with Yahoo Smushit
    smushit:
      dist:
        src: '<%= cfg.dist.asset %>/img/**/*.{png,jpg,jpeg}'

    # Minify SVG using SVGO
    svgmin:
      dist:
        expand: true
        cwd: '<%= cfg.dist.asset %>/img'
        dest: '<%= cfg.dist.asset %>/img'
        src: [ '**/*.svg' ]

    # ---------------------------------
    # Helpers
    copy:
      img:
        expand: true
        filter: 'isFile'
        cwd: '<%= cfg.src %>/images'
        dest: '<%= cfg.dist.asset %>/img'
        src: [ '**/*.{png,jpg,jpeg,gif,svg}' ]
      font:
        expand: true
        flatten: true
        filter: 'isFile'
        cwd: '.'
        dest: '<%= cfg.dist.asset %>/font/'
        src: [
          '<%= cfg.src %>/fonts/*',
          'bower_components/font-awesome/font/*'
        ]
      component:
        files: [
          # Polyfill
          # { src: 'bower_components/box-sizing-polyfill/boxsizing.htc', dest: '<%= cfg.dist.asset %>/css/boxsizing.htc' },
          # JS libs
          { src: 'bower_components/jquery/jquery.min.js', dest: '<%= cfg.dist.asset %>/js/libs/jquery.<%= cpt.dependencies.jquery %>.min.js' },
          { src: 'bower_components/modernizr/modernizr.js', dest: '<%= cfg.dist.asset %>/js/libs/modernizr.dev.js' }
        ]

    clean:
      options:
        force: true
      dev: [
        '<%= concat.mainJs.dest %>',
        '<%= cfg.dist.asset %>/js/libs/modernizr.dev.js',
        '<%= cfg.dist.asset %>/css/main.css',
        '<%= cfg.dist.root %>/test.html'
      ]
      img: '<%= cfg.dist.asset %>/img'
      font: '<%= cfg.dist.asset %>/font'
      all: [
        '<%= cfg.dist.root %>/*.{txt,ico,appcache}',
        '<%= cfg.dist.root %>/crossdomain.xml',
        '<%= cfg.dist.root %>/{404,test}.html',
        '<%= cfg.dist.asset %>/{js,css,img,font}'
      ]

    replace:
      options:
        force: true
        variables:
          'authorName': '<%= pkg.author.name %>'
          'authorEmail': '<%= pkg.author.email %>'
          'authorSite': '<%= pkg.author.url %>'
          'title': '<%= pkg.title %>'
          'version': '<%= pkg.version %>'
          'homepage': '<%= pkg.homepage %>'
          'timestamp': '<%= grunt.template.today("dd-mm-yyyy") %>'
      file:
        files: [
          expand: true
          cwd: '<%= cfg.src %>/files'
          dest: '<%= cfg.dist.root %>'
          src: [ '**/!(_)**' ]
        ]
      doc:
        files: [
          expand: true
          cwd: '<%= cfg.doc %>'
          dest: '<%= cfg.doc %>'
          src: [ 'index.html' ]
        ]

    # ---------------------------------
    # Analyse css files and log simple metrics
    cssmetrics:
      css:
        src: [ '<%= cfg.dist.asset %>/css/*.css' ]

    # Log js files sizes
    bytesize:
      js:
        src: [ '<%= cfg.dist.asset %>/js/*.js', '<%= cfg.dist.asset %>/js/libs/modernizr.*.js' ]

    # ---------------------------------
    # Bump the version package
    bump:
      options:
        files: [ 'package.json', 'bower.json' ]
        push: false

    # ---------------------------------
    # API Documentation with YUIDoc
    yuidoc:
      compile:
        name: '<%= pkg.title %> | API Documentation'
        description: '<%= pkg.description %>'
        version: '<%= pkg.version %>'
        url: '<%= pkg.homepage %>'
        options:
          paths: '<%= cfg.src %>/scripts/'
          outdir: '<%= cfg.doc %>/yuidoc/<%= pkg.version %>/'

    # Compile doc.jade
    jade:
      doc:
        options:
          pretty: true
          data:
            pkg: '<%= pkg %>'
        files: '<%= cfg.doc %>/documentation-v<%= pkg.version %>.html': '<%= cfg.src %>/doc.jade'

    # ---------------------------------
    # Watch for changes
    watch:
      options:
        livereload: true
      scripts:
        files: '<%= cfg.src %>/scripts/**/*.js'
        tasks: [ 'concat', 'gjslint', 'jshint' ]
      styles:
        files: '<%= cfg.src %>/styles/**/*.styl'
        tasks: [ 'stylus', 'csscomb', 'csslint' ]
      images:
        files: '<%= cfg.src %>/images/**/*.{png,jpg,jpeg,gif,svg}'
        tasks: [ 'clean:img', 'copy:img' ]
      fonts:
        files: '<%= cfg.src %>/fonts/**/*'
        tasks: [ 'clean:font', 'copy:font' ]
      config:
        files: [ 'Gruntfile.coffee', '*.json', '.{csslintrc,jshintrc}' ]
        tasks: [ 'build' ]
      livereload:
        files: '<%= cfg.dist.root %>/**/*.{html,php}'

  # =================================
  # Helpers
  copyFile = (src, dst) ->
    unless grunt.file.exists dst
      grunt.file.copy src, dst

  # =================================
  # Tasks
  grunt.registerTask 'default', [
    'build',
    'watch'
  ]

  grunt.registerTask 'build', 'Copy/Compile/Lint/(Minify) sources files.', (env) ->
    grunt.task.run [
      'clean:all',
      'replace:file',
      'copy',
      'concat',
      'gjslint',
      'jshint',
      'bytesize',
      'stylus',
      'csscomb',
      'csslint',
      'cssmetrics'
    ]
    if env is 'prod'
      grunt.task.run [
        'modernizr',
        'uglify',
        'bytesize',
        'cssmin',
        'cssmetrics',
        'imagemin',
        'smushit',
        'svgmin',
        'clean:dev'
      ]

  grunt.registerTask 'test', 'Setup Intern and run tests.', () ->
    copyFile 'node_modules/intern/tests/example.intern.js', pathConfig.test + '/intern.js'
    grunt.task.run [
      'intern:client'
    ]

  grunt.registerTask 'doc', 'Setup basic html documentations.', () ->
    cfg = pathConfig
    assetDoc = [
      # { src: cfg.app + '/files/favicon.ico', dst: cfg.doc + '/favicon.ico' },
      { src: 'bower_components/bootstrap/dist/js/bootstrap.min.js', dst: cfg.doc + '/assets/bootstrap.min.js' },
      { src: 'bower_components/bootstrap/dist/css/bootstrap.min.css', dst: cfg.doc + '/assets/bootstrap.min.css' },
      { src: 'bower_components/jquery/jquery.min.js', dst: cfg.doc + '/assets/jquery.min.js' }
    ]
    for fls in assetDoc
      copyFile fls.src, fls.dst
    indexDoc = '''
    <!DOCTYPE html><html><head><title>@@title | Documentation</title>
    <meta http-equiv="refresh" content="0;url=documentation-v@@version.html" />
    </head><body><p>Redirected to <a href="documentation-v@@version.html">Documentation v@@version</a>.</p></body></html>
    '''
    grunt.file.write cfg.doc + '/index.html', indexDoc
    grunt.task.run [
      'yuidoc',
      'jade:doc',
      'replace:doc'
    ]
