# [BP](ur://github.com/bymathias/bp "BP") [![Dependency Status](https://david-dm.org/bymathias/bp.png)](https://david-dm.org/bymathias/bp) [![devDependency Status](https://david-dm.org/bymathias/bp/dev-status.png)](https://david-dm.org/bymathias/bp#info=devDependencies)

## Introduction

Front end boilerplate/framework and build process to quickly get projects going. [Thanks to..](https://github.com/bymathias/bp/blob/master/app/files/humans.txt#L26 "Thanks to..")

## Requirements

[Git](http://git-scm.com "Git"), [Php](https://github.com/csscomb/CSScomb/wiki/Requirements "Required for CSScomb"), [PhantomJS](http://phantomjs.org "PhantomJS") and [Node.js](http://nodejs.org/ "Node.js"), install the [npm](https://npmjs.org "Node Packaged Modules") modules required globally:

```
npm install -g grunt-cli bower stylus
```

## Installation

Clone the repository and install dependencies

```
git clone --depth=1 https://bymathias@github.com/bymathias/bp.git project
cd !$

npm install
bower install
```

## Usage

```
grunt
grunt build:<dev|prod>

grunt test
grunt bump:<patch|minor|major>
grunt doc
```
see the [Gruntfile](https://github.com/bymathias/bp/blob/master/Gruntfile.coffee) or `grunt -h`.

## License

- Third-party libraries, licensed under their respective licenses.
- Custom bits, licensed under the [MIT License](https://github.com/bymathias/bp/blob/master/LICENSE.md "Copyright-2013 bp").
