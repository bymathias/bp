// MAIN Scripts


var AppByse = (function() {
    'use strict';

    var app,
        config;

    config = {
        logMsg: 'Up and running !'
    };

    app = {
        // Initialization logic
        init: function(options) {
            // Custom config via init()
            $.extend(config, options);

            var cf = config;
            console.log(cf.logMsg);
        },

        // Some logic
        doStuff: function() {

        }

    };

    return app;

})();


// DOM is ready
$(document).ready(function() {
    // Fire up
    AppByse.init();
});
